import { defineConfig } from 'vitepress';

const GitLabIcon = `<svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-brand-gitlab"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" fill="none" /></svg>`;

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Soapbox Docs",
  description: "Powering the future of social media.",
  appearance: {
    // @ts-expect-error not fully supported yet
    initialValue: 'light'
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: '/img/soapbox.svg',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Soapbox', link: '/soapbox/', activeMatch: '^\/soapbox\/' },
      { text: 'Ditto', link: '/ditto/', activeMatch: '^\/ditto\/' },
      { text: 'Mostr', link: '/mostr/', activeMatch: '^\/mostr\/' },
      { text: 'soapbox.pub', link: 'https://soapbox.pub' },
    ],
    sidebar: {
      '/soapbox/': {
        base: '/soapbox/',
        items: [
          { text: 'Overview', link: '/' },
          { text: 'Customization', link: 'customization' },
          { text: 'Translations', link: 'translations' },
          {
            text: 'Install',
            base: '/soapbox/install/',
            items: [
              { text: 'Pleroma', link: 'pleroma' },
              { text: 'Mastodon', link: 'mastodon' },
            ],
          },
          {
            text: 'Administration',
            base: '/soapbox/administration/',
            items: [
              { text: 'Updating', link: 'updating' },
              { text: 'Removing', link: 'removing' },
              { text: 'Deploy at Scale', link: 'deploy-at-scale' },
            ],
          },
          {
            text: 'Development',
            base: '/soapbox/development/',
            items: [
              { text: 'How it Works', link: 'how-it-works' },
              { text: 'Running Locally', link: 'local' },
              { text: 'Yarn Commands', link: 'yarn-commands' },
              { text: 'Build Configuration', link: 'build-config' },
              { text: 'Developing a Backend', link: 'backend' },
            ],
          },
        ],
      },
      '/ditto/': {
        base: '/ditto/',
        items: [
          { text: 'Ditto', link: '/' },
          { text: 'Install', link: 'install' },
          { text: 'Syncing Events', link: 'sync' },
          // { text: 'Docker', link: 'docker' },
          { text: 'Useful Commands', link: 'cli' },
          { text: 'Known Issues', link: 'known-issues' },
          { text: 'Troubleshooting', link: 'troubleshooting' },
          {
            text: 'Configuration',
            items: [
              { text: 'Moderation Policies', link: 'policies' },
              { text: 'Media Attachments', link: 'media' },
              { text: 'Environment Variables', link: 'env' },
              { text: 'Rate Limiting', link: 'rate-limiting' },
              { text: 'Error Monitoring', link: 'monitoring' },
              { text: 'Prometheus Metrics', link: 'metrics' },
              { text: 'Translations', link: 'translations' },
            ],
          },
          {
            text: 'Internals',
            items: [
              { text: 'Ditto API', link: 'api' },
              { text: 'Ditto nsec', link: 'nsec' },
              { text: 'Signing Events', link: 'signing' },
              { text: 'Firehose', link: 'firehose' },
              { text: 'Pipeline', link: 'pipeline' },
            ],
          },
        ],
      },
      '/mostr/': {
        base: '/mostr/',
        items: [
          { text: 'Mostr', link: '/' },
          { text: 'Troubleshooting', link: 'troubleshooting' },
        ],
      },
    },
    socialLinks: [
      { icon: { svg: GitLabIcon }, link: 'https://gitlab.com/soapbox-pub' },
    ],
    editLink: {
      pattern: 'https://gitlab.com/soapbox-pub/soapbox-docs/-/blob/main/:path',
    },
    search: {
      provider: 'local',
    },
  },
  head: [
    ['link', { rel: 'icon', href: '/img/favicon.ico' }],
    ['meta', { property: 'og:image', content: 'https://docs.soapbox.pub/img/social-card.png' }],
    ['meta', { name: 'twitter:image', content: 'https://docs.soapbox.pub/img/social-card.png' }],
  ],
  cleanUrls: true,
  lastUpdated: true,
  markdown: {
    languageAlias: {
      promql: 'coffee',
    },
  },
})
