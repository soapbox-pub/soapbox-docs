# Soapbox Docs

Documentation for Soapbox, built on [VitePress](https://vitepress.dev/).

## Local development

```sh
npm install
npm run dev
```