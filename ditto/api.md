# Ditto API

Ditto implements the [Mastodon API](https://docs.joinmastodon.org/methods/) and parts of the [Pleroma API](https://api.pleroma.social/). It aims to be compatible with existing Mastodon clients.

## Signing Events

Endpoints that create Nostr events (eg `POST /api/v1/statuses`) will hang for up to 60 seconds before returning a response. During this time, a remote signer must sign the event, or else the API will return an error.

See [Signing on Ditto](./signing.md).

## Entity IDs

Entities use Nostr hex identifiers for their IDs when applicable.

Statuses use the event `id`, and accounts use the `pubkey`.

## Usernames

The Account `acct` is the fully-qualified [NIP-05](https://github.com/nostr-protocol/nips/blob/master/05.md) identifier if it's valid, otherwise it's the npub.

The `username` is the localpart of the NIP-05 identifier if it's valid, otherwise it's the npub.
