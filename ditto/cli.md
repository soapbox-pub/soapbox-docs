# Useful Commands

This section has a list of useful commands, some of which you will be running often.

## Promoting a user to admin

Run this command as the `ditto` user inside `/opt/ditto`:

```sh
deno task admin:role <hex-pubkey> admin
```

To demote an admin, run the same command, replacing `admin` with `user` at the end.

## Install a specific version of Deno

Although Deno has been around for a while, it's still "cutting edge" by comparison to Node. If you have inadvertently upgraded Deno and it broke things, you can install a specific version:

```sh
curl -fsSL https://deno.land/x/install/install.sh | sudo DENO_INSTALL=/usr/local sh -s <version>
```

Replace `<version>` at the end with the version you want to install. For example, `v1.43.3`.

## Tasks

Tasks are bite-sized pieces of Ditto's functionality, used to accomplish tasks such as cleaning up the database, exporting or importing events, etc. You may remember the `deno task` commands from installation. These are tasks too!

### Enforcing a policy retroactively

Ditto's policy features enable you to filter out certain kinds of Nostr events, keeping your server free of unwanted or objectionable content. But what if you want to apply a policy to the events already in the Ditto database? This might be useful in case of, for example, a spam attack on your server.

To run all existing events through the currently configured (by setting the  DITTO_POLICY environment variable to a valid policy module) policy, simply run:

```sh
deno task db:cleanup
```

## Nginx commands

If you [followed our tutorial](./install.md), you may want to know the following commands:  
- Each time you change your `/etc/nginx/sites-enabled/ditto.conf` file, you need to restart nginx.

```sh
sudo systemctl reload nginx
sudo systemctl restart nginx
```

- To pull for the latest changes in your Ditto instance:

```sh
cd /opt/ditto/
su ditto
git pull
exit
```

- Each time you change something in `/opt/ditto/`, you need to reload the system unit:

```sh
systemctl daemon-reload
systemctl restart ditto.service
```

- See logs of Ditto in real time:

```sh
sudo journalctl -u ditto.service -f
```

- See past logs of Ditto:

```sh
sudo journalctl -u ditto.service
```

## Debugging Ditto

Setting `DEBUG="*"` will log almost everything being executed in real time, such as the SQL queries, HTTPs requests with METHOD, response status, lag time, streaming requests and so on.

In local development, you can pass `DEBUG` to the command line:

```sh
DEBUG="*" deno task dev
```

In production, you can add the variable to your `.env`:

```sh
DEBUG="*"
```