# Environment Variables

Ditto uses the following environment variables to configure its behavior:

## Essential

These following variables are required:

- `DITTO_NSEC` - Ditto secret key for internal use. Site configuration, NIP-05 grants, moderation events, and more are signed with this key.

## Server

These variables configure the HTTP server:

- `PORT` - Port for the HTTP server to listen on. Defaults to `4036`.

- `LOCAL_DOMAIN` - Domain name for the Ditto server, ex `https://example.com`. Defaults to `http://localhost:4036`.

## Database

- `DATABASE_URL` - URL for the Postgres database. Supports `postgres://`, `file://` and `memory://` protocols. Defaults to `file://data/pgdata`. File and memory use [PGlite](https://pglite.dev/), but it is recommended to use a real Postgres database in production.

- `PG_POOL_SIZE` - Number of connections to the Postgres database. Defaults to `10`.

### Timeouts

- `DB_TIMEOUT_DEFAULT` - Default timeout for database queries in milliseconds. Defaults to `5000`.

- `DB_TIMEOUT_RELAY` - Timeout for relay queries in milliseconds. Defaults to `1000`.

- `DB_TIMEOUT_TIMELINES` - Timeout for timelines in the API, eg home, notifications, hashtag timelines, and account timelines. Defaults to `15000`.

## Miscellaneous

- `FIREHOSE_ENABLED` - Whether to connect to continuously stream data from other relays. Defaults to `true`.

- `DITTO_POLICY` - Policy plugin import. See [Policies](./policies.md) for more information.