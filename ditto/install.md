---
outline: deep
---

# Installing Ditto

This guide will teach you how to install Ditto on a Linux VPS (we recommend Ubuntu) 
using [deno](https://deno.com/), [systemd](https://systemd.io/),
and [nginx](https://nginx.org/en/).

## Prerequisites

### Technical skill level
At this stage in Ditto's development, we recommend some baseline technical knowledge before diving into Ditto including: comfort with the **terminal on Linux** 
(including installing pacakges, etc), familiarity with the **basics of Nostr**, ability to configure **domain names**, and a basic understanding of **database management**. 
However, we are working hard to make running Ditto more accesible to everyone in the future!

### Server requirements
The specific hardware your server needs depends on your use case. 
In general, we recommend starting with 4 cores, 8gb ram, and 100gb disk.

### Domain name
Prior to setting up your Ditto server, you should already own the domain where your Ditto will be hosted and have it pointed to your server.

## 1. System setup

### 1.a. Install updates

Usually a fresh VPS already has outdated software, so run the following commands to update it:

::: code-group
```sh [Ubuntu]
sudo apt update
sudo apt upgrade
```
```sh [Fedora]
sudo dnf check-update
sudo dnf update
```
```sh [Arch]
sudo pacman -Syu
```
:::

When prompted (`[Y/n]`) type `Y` and hit Enter.

### 1.b. Install system dependencies

Ditto relies on some additional system software in order to function. Install them with the following command:

::: code-group
```sh [Ubuntu]
sudo apt install git curl unzip nginx postgresql-contrib certbot python3-certbot-nginx
```
```sh [Fedora]
sudo rpm install git curl unzip nginx postgresql-contrib certbot python3-certbot-nginx
```
```sh [Arch]
sudo pacman -S git curl unzip nginx postgresql-contrib certbot certbot-nginx
```
:::

### 1.c. Install Deno

To install Deno system-wide, run:

```sh
curl -fsSL https://deno.land/x/install/install.sh | sudo DENO_INSTALL=/usr/local sh
```

When prompted `Edit shell configs to add deno to the PATH? (Y/n)` hint Enter twice to continue.

### 1.d. Create the Ditto user

For security reasons, it’s best to run Ditto as a separate user with limited access.

We’ll create this user and call it `ditto`:

```sh
sudo adduser ditto
```

Then, hit Enter to skip adding additional user details. 

## 2. Install Ditto

It’s time to install Ditto. Let’s get things up and running.

### 2.a. Download source code

```sh
git clone https://gitlab.com/soapbox-pub/ditto /opt/ditto
chown -R ditto:ditto /opt/ditto
```

Enter the source code directory, and become the ditto user:

```sh
cd /opt/ditto
sudo su ditto
```

### 2.b. Configure Ditto

Run the following command to create a `.env` file:

```sh
deno task setup
```

Follow the instructions in the terminal to set up your `.env` file:

- `What is the domain of your instance? (eg ditto.pub)` : Enter your domain
- `Which database do you want to use?` : Postgres
- `Postgres host` : localhost
- `Postgre port` : 5432
- `Postgres user` : ditto
- `Postgres password` Enter your password and save it for later.
- `Postgres database` : ditto
- `How do you want to upload files?` : Choose your uploader, default to nostrbuild
- `Build endpoint` : Hit enter to select default

> [!TIP] 
> If you choose to use the local uploader for more control over uploaded files, you'll have to set up an additional nginx rule. See "Configure nginx" below.

### 2.c. Add Soapbox

Ditto can serve any frontend (or none at all), but we recommend using Soapbox for the best experience.

Run this command to install Soapbox:

```sh
deno task soapbox
```

> [!TIP]
> You can re-run this command at any time to update Soapbox to the latest version.

### 2.d. Provision the database

If you are still the `ditto` user, exit the shell:

```sh
exit
```

Create a new Postgres user and database for Ditto:

```sh
sudo -u postgres createuser -P ditto
sudo -u postgres createdb ditto -O ditto
```

When prompted `Enter password for new role:` enter the password you used for the ditto user in step 2.b.

### 2.e. Start Ditto

Copy the systemd unit file to the correct location:

```sh
sudo cp /opt/ditto/installation/ditto.service /etc/systemd/system/ditto.service
```

Reload the systemd manager configuration:

```sh
sudo systemctl daemon-reload
```

Enable the Ditto service to start automatically at boot and start it immediately:

```sh
sudo systemctl enable --now ditto
```

Now Ditto is running. You're almost done!

## 3. Getting online

The last step is to make your server accessible to the outside world. We’ll achieve that by installing Nginx and enabling HTTPS support.

### 3.a. Configure Nginx

Copy the Nginx configuration file to the correct location:

```sh
sudo cp /opt/ditto/installation/ditto.conf /etc/nginx/sites-enabled/ditto.conf
```

Next edit the configuration file to replace `example.com` with your domain name:

```sh
sudo nano /etc/nginx/sites-enabled/ditto.conf
```

Edit this line:
```
server {
  server_name example.com;
```
Then save and exit with `CTRL+X` , then `Y` , then `ENTER`


#### 3.b.i. Setting up nginx to serve local uploads

If you chose "Local" as the uploader above, you need to set up nginx to serve local uploads from the directory you selected.

```nginx
server {
    server_name media.your.domain;

    location / {
        root </path/to/serve/local/files/from>;
        autoindex off;  # Optional: allows directory listing. We recommend keeping this off.
        try_files $uri $uri/ =404;
    }

    error_page 404 /404.html;
    location = /404.html {
        internal;
    }
}
```

### 3.b. Obtain an SSL certificate

Run the following command to obtain an SSL certificate from Let’s Encrypt:

```sh
sudo certbot --nginx
```

Follow the prompts to obtain the certificate.

### 3.c. Restart Nginx

Restart Nginx to apply the changes:

```sh
sudo systemctl restart nginx
```

🎉 Congrats, you’re done! Check your site in a browser and it should be online.

## 4. Sync your existing data

> [!TIP]
>
> Nak, by fiatjaf, is an essential tool for managing a Nostr relay like Ditto. Install it from [Github](https://github.com/fiatjaf/nak).

If you are already a Nostr user, you need to copy your existing profile data to Ditto before you use Ditto, or it could overwrite your profile. You can also get your friends' data and posts onto your Ditto instance. The more the merrier!

On the server that is running ditto, run the following command:

(If you don't want to import posts and only get profiles, you can run it without `-k 1`)

```sh
nak req -a [YOUR HEX NPUB HERE] -k 0 -k 1 [RELAY WHERE YOUR NPUB CAN BE FOUND] | nak event ws://localhost:4036/relay
```

For example: 

```sh
nak req -a 932614571afcbad4d17a191ee281e39eebbb41b93fac8fd87829622aeb112f4d relay.primal.net | nak event ws://localhost:4036/relay
```

> [!TIP]
>
> Convert your npub to hex npub with `nak decode [NPUB]`.
> For example: 
> ```sh
> ➜  ~ nak decode npub1jvnpg4c6ljadf5t6ry0w9q0rnm4mksde87kglkrc993z46c39axsgq89sc
> {
>   "pubkey": "932614571afcbad4d17a191ee281e39eebbb41b93fac8fd87829622aeb112f4d"
> }
> ```


## 5. Post-installation

Below are some additional steps you can take after you’ve finished installation.

### Promote a user to admin

To promote a user to admin, `cd` into the Ditto directory and run the following command:

```sh
deno task admin:role <pubkey> admin
```

### Add relays

To get data into your server, you need to add relays. Ditto will store all data streamed from these relays.

In Soapbox, navigate to "Dashboard" > "Relays" and add the relays you want to ingest events from. Restart the Ditto server (`systemctl restart ditto`) for the changes to take effect.

> [!WARNING]
> Keep the relay list small to avoid performance issues. We suggest using **1-3 relays.**  
> Please see [Known Issues](/ditto/known-issues) for more information.

### Sync events

See [Syncing Events](/ditto/sync) for information on how to get data from the network into your new server.

### Update Ditto

To update Ditto, pull the latest changes from the repository and restart the service:

```sh
cd /opt/ditto
su ditto
git pull origin main
exit
sudo systemctl restart ditto
```

### Renew SSL certificate

To renew the SSL certificate, run:

```sh
sudo certbot renew
```

### Useful commands

Check [Useful Commands](/ditto/cli) for additional options.

### Troubleshooting

Something not working right? First try [Troubleshooting Ditto](/ditto/troubleshooting). If you can’t find a solution there, please [open an issue](https://gitlab.com/soapbox-pub/ditto/-/issues/new) and we’ll help you out.
