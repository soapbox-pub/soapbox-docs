# Known Issues

This document lists notable issues with Ditto at the time of launch.

## Some profiles or posts are missing

Ditto reads data from its own database. If you add a list of relays (by visiting "Dashboard" > "Relays" in Soapbox), Ditto will ingest and store all events streamed from those relays. However, some data (especially profiles) may still be missing.

Ditto currently lacks outbox support, so it cannot resolve missing profiles or posts on its own.

If you would like to manually populate the database, you can use [`nak`](https://github.com/fiatjaf/nak) to copy data from other relays to your Ditto instance.

For example, to copy a user's profile from nostr.band, run the following command:

```sh
nak req -k 0 -a <missing-user-pubkey> wss://relay.nostr.band | nak event wss://<my-ditto-domain>/relay
```

To copy a bunch of profiles at once, use `-k 0` and don't specify any pubkey:

```sh
nak req -k 0 wss://relay.nostr.band | nak event wss://<my-ditto-domain>/relay
```

See [Sync your data between relays with nak](https://habla.news/u/alex@gleasonator.dev/nak-sync) for more advice.
