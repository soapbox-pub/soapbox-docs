# Media Uploads

To enable media uploads on your server, set the `DITTO_UPLOADER` environment variable.
It supports the following values:

- `nostrbuild` - Uploads media to AWS servers (free, secure, no authentication required).
- `ipfs` - Uploads media to a self-hosted IPFS installation.
- `s3` - Uploads media to an S3-compatible bucket.
- `local` - Saves media to the local filesystem.
- `blossom` - Uploads to publicly accessible servers.

::: tip
It is recommended to store uploads on a separate server from Ditto itself, or else Ditto may run out
of disk space and stop working.
:::

## Media Domain

By default, Ditto expects that media will be served on `media.yoursite.tld`, which it derives from the
`LOCAL_DOMAIN`. You can override this by setting the `MEDIA_DOMAIN` environment variable.

> [!NOTE]
> Media **must** be hosted on a separate domain or subdomain to prevent security issues.
> Ditto will raise an error if you try to serve media from the same domain as Ditto itself.

## nostr.build
Just set the `DITTO_UPLOADER` environment variable to `nostrbuild`.  
You can learn more about nostr.build here: https://nostr.build/

## IPFS

To set up IPFS, set `DITTO_UPLOADER=ipfs` and install [kubo](https://docs.ipfs.tech/how-to/command-line-quick-start/).

If Ditto and kubo are on the same machine, then your task is done. :tada:

If kubo is on a different machine, you will need to enable local networking, and then customize 
the `IPFS_API_URL` in Ditto.

### IPFS Variables

- `DITTO_UPLOADER=ipfs`
- `IPFS_API_URL` - The URL of the IPFS API server. Defaults to `http://localhost:5001`.

## S3

To enable S3, set `DITTO_UPLOADER=s3`. You can use any S3-compatible service, such as Wasabi,
DigitalOcean Spaces, MinIO, and more.

You will need to configure S3 with some additional variables, and it may vary depending on the provider.

### S3 Variables

- `DITTO_UPLOADER=s3`
- `S3_ENDPOINT` - The S3 endpoint URL.
- `S3_REGION` - The S3 region.
- `S3_ACCESS_KEY` - The S3 access key.
- `S3_SECRET_KEY` - The S3 secret key.
- `S3_BUCKET` - The name of the S3 bucket.
- `S3_PATH_STYLE` - Set to `true` if your S3 provider uses path-style URLs.
- `S3_PORT` - The port to use for S3.
- `S3_SESSION_TOKEN` - The S3 session token.
- `S3_USE_SSL` - Set to `false` to disable SSL.

## Local

To save media to the local filesystem, set `DITTO_UPLOADER=local`.

You will then need to configure a reverse-proxy like Nginx to serve the media files.

> [!WARNING]
> - This method is insecure.
> - This method can cause your server to quickly run out of disk space.
> - Do not serve media from the same domain as the Ditto API! This can lead to security issues.
> - It is highly recommended to use IPFS or S3 instead.

### Local Variables

- `DITTO_UPLOADER=local`
- `UPLOADS_DIR` - The directory to save uploads to. Defaults to `data/uploads`.

## Blossom

Just set the `DITTO_UPLOADER` environment variable to `blossom` and the `BLOSSOM_SERVERS`
variable to a Blossom server, such as `https://blossom.primal.net/`.  
:::tip
You can specify multiple blossom servers by separating them with a comma.
:::
