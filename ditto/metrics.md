# Prometheus Metrics

Ditto exposes an endpoint at `/metrics` that returns data in [*Prometheus exposition format*](https://prometheus.io/docs/instrumenting/exposition_formats/) (aka [OpenMetrics format](https://github.com/OpenObservability/OpenMetrics/blob/main/specification/OpenMetrics.md)). This data can be scraped by Prometheus to create real-time graphs of Ditto.

[OpenMetrics]: https://openmetrics.io/

> [!INFO]
> Prometheus was created by SoundCloud in 2012. It became so ubiquitous that [OpenMetrics] sought to standardize its custom data format.

## Installing Prometheus

Install Prometheus from your package manager:

::: code-group
```sh [Ubuntu]
sudo apt install prometheus
```
```sh [Fedora]
sudo dnf install prometheus
```
```sh [Arch]
sudo pacman -S prometheus
```
:::

### Scrape Metrics from Ditto

Now we need to configure Prometheus to scrape metrics from Ditto. Let's edit the Prometheus configuration file:

```sh
sudo nano /etc/prometheus/prometheus.yml
```

Add the following job to the `scrape_configs` section:

```yml{3-6}
scrape_configs:
  # ... other jobs
  - job_name: 'ditto'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:4036']
```

Save the file and restart Prometheus:

```sh
sudo systemctl restart prometheus
```

### Accessing the Dashboard

Rather than expose Prometheus to the public internet, we can use an SSH tunnel. From your local machine, run:

```sh
ssh -L 9090:localhost:9090 user@your_server_ip
```

Then, open your browser and navigate to `http://localhost:9090`.

That's it! Now try running some queries.

## Add the Ditto Dashboard

Ditto comes with a pre-configured dashboard for Grafana, located in [`grafana/Ditto-Dashboard.json`](https://gitlab.com/soapbox-pub/ditto/-/blob/main/grafana/Ditto-Dashboard.json) of the respository.

In Grafana, add a new dashboard and then choose "Import". Select the JSON file and configure the data sources to use it.

![Ditto Grafana dashboard](./img/grafana.png)

## Useful Queries

Here are some useful queries that you can run in Prometheus:

### Available Database Connections

```promql
ditto_db_available_connections
```

### HTTP Error Responses by Path

```promql
rate(ditto_http_responses_total{status=~"5.."}[5m])
```

### Active Relay Connections

```promql
ditto_relay_connections
```

### Relay Messages by Verb

```promql
rate(ditto_relay_messages_total[5m])
```

### Relay Events by Kind

```promql
rate(ditto_relay_events_total[5m])
```

### Firehose Events by Kind

```promql
rate(ditto_firehose_events_total[5m])
```

### Database Events by Kind

```promql
rate(ditto_db_events_total[5m])
```

### Fetch Requests

```promql
rate(ditto_fetch_total[5m])
```

### HTTP Responses by Path

```promql
rate(ditto_http_responses_total[5m])
```

### HTTP Requests by Method

```promql
rate(ditto_http_requests_total[5m])
```
