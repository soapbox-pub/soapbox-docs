# Ditto Error Monitoring

If you would like to be notified whenever errors occur in Ditto, you can set up error monitoring.
This will provide stack traces and other useful information to help us debug issues.

Ditto can use [Sentry](https://sentry.io) or [GlitchTip](https://glitchtip.com) for error monitoring.

## Create an Account

If you don't already have an account, you can create one at [Sentry](https://sentry.io) or [GlitchTip](https://glitchtip.com).

## Configure Ditto

Ditto uses the `SENTRY_DSN` environment variable to configure error monitoring. This variable should be set to the DSN provided by Sentry or GlitchTip.

Modify `.env` to include the following line:

```sh
SENTRY_DSN="https://your-dsn-here@your-sentry-or-glitchtip-url"
```

## Viewing Errors

Once you have set up error monitoring, you can view errors in the Sentry or GlitchTip dashboard.

These services also provide email notifications for new errors, so you can be alerted as soon as an issue occurs.