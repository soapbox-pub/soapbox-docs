# Ditto Pipeline

The Ditto pipeline is a function which all events pass through, regardless of their origin. It is responsible for processing, filtering, and transforming events before they are saved to the database.

The pipeline is responsible for:

- **Moderation**: Filtering out spam, NSFW, or other unwanted content according to [policies](/ditto/policies).
- **Saving**: Storing events in the database.
- **Trends**: Calculating trends, such as trending hashtags.
- **Feeds**: Updating feeds, such as user timelines.
- **Streaming**: Sending events to Mastodon clients and the Nostr relay in real-time.