# Ditto Moderation Policies

Policies are custom scripts used to reject or accept events Ditto.
If an event is rejected, it will be ignored, won't be saved in Ditto, and won't be shown to users.

## Custom Script

By placing a TypeScript file at `data/policy.ts`, you can write a custom policy in TypeScript.

Example policy:

```ts
import { NPolicy, NostrEvent, NostrRelayOK } from '@nostrify/nostrify';
import { AntiDuplicationPolicy, KeywordPolicy, PipePolicy } from '@nostrify/nostrify/policies';

const kv = await Deno.openKv();

export default class AppPolicy implements NPolicy {
  async call(event: NostrEvent): Promise<NostrRelayOK> {
    const policy = new PipePolicy([
      new KeywordPolicy(['https://t.me/']),
      new AntiDuplicationPolicy({ kv, minLength: 50 }),
    ]);

    return policy.call(event);
  }
}

```

The default export must contain a `call` function that takes an event and returns a promise with a message like `['OK', event.id, true, '']` to accept the event, or `['REJECT', event.id, false, 'blocked: event did not meet criteria']` to reject it.

Policy classes can be used from [Nostrify](https://nostrify.dev/policy/).

## Remote Policies

You can also download and share policies from anywhere on the net! Just set:

```sh
DITTO_POLICY="<policy-url>"
```

:::warning
Policies execute JavaScript code on your server. Only use policies from trusted sources.
:::

### From a URL

Policies can be added from GitHub Gists, or any other website.

```sh
DITTO_POLICY="https://example.com/policy.ts"
```

### From JSR

Policies can also be imported from [JSR](https://jsr.io/).

```sh
DITTO_POLICY="jsr:@gleasonator/policy"
```

### Community Policies

- [`jsr:@gleasonator/policy`](https://jsr.io/@gleasonator/policy) - policy used by gleasonator.dev