# Rate Limiting

Ditto uses the `X-Real-IP` header to identify the IP address of the client. This header is set by the reverse proxy in front of Ditto. If the header is not present, rate limiting will not be applied.

## With Nginx

If you followed the [install guide](./install.md), you should have the following line in your Nginx config:

```nginx
proxy_set_header X-Real-IP $remote_addr;
```

This is all that is needed to make rate limiting work.

## With Cloudflare

Since Cloudflare is an additional proxy above Nginx, it requires extra steps to make rate limiting work.

Create the file `/etc/nginx/cloudflare` with the following content:

```nginx
set_real_ip_from 173.245.48.0/20;
set_real_ip_from 103.21.244.0/22;
set_real_ip_from 103.22.200.0/22;
set_real_ip_from 103.31.4.0/22;
set_real_ip_from 141.101.64.0/18;
set_real_ip_from 108.162.192.0/18;
set_real_ip_from 190.93.240.0/20;
set_real_ip_from 188.114.96.0/20;
set_real_ip_from 197.234.240.0/22;
set_real_ip_from 198.41.128.0/17;
set_real_ip_from 162.158.0.0/15;
set_real_ip_from 104.16.0.0/13;
set_real_ip_from 104.24.0.0/14;
set_real_ip_from 172.64.0.0/13;
set_real_ip_from 131.0.72.0/22;
set_real_ip_from 2400:cb00::/32;
set_real_ip_from 2606:4700::/32;
set_real_ip_from 2803:f800::/32;
set_real_ip_from 2405:b500::/32;
set_real_ip_from 2405:8100::/32;
set_real_ip_from 2a06:98c0::/29;
set_real_ip_from 2c0f:f248::/32;

real_ip_header CF-Connecting-IP;
```

> [!TIP]
> These values are taken from the [Cloudflare IP Ranges](https://www.cloudflare.com/ips/) which were last updated at Sep 28, 2023 at the time of writing.

Next edit `/etc/nginx/nginx.conf` and make the following change:

```nginx
	##
	# Virtual Host Configs
	##

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;

	## // [!code ++]
	# Cloudflare Real IP // [!code ++]
	## // [!code ++]

	include /etc/nginx/cloudflare; # [!code ++]
}
```

Finally, restart Nginx:

```sh
sudo systemctl restart nginx
```