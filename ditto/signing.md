# Signing on Ditto

![Ditto Signing](./img/ditto-sign.svg)

Ditto implements [NIP-46](https://github.com/nostr-protocol/nips/blob/master/46.md) remote signing.

When Mastodon clients authorize with Ditto, the user needs to enter a `bunker://` URI into the OAuth form. Ditto then generates a NIP-46 "local keypair" on the server, and then grants the Mastodon client a bearer token.

When users need to sign Nostr events (eg when posting a status), they send the bearer token to Ditto's API. Ditto then emits a NIP-46 request on its relay, signed by the generated keypair, tagging the user's pubkey.

The remote signer must sign the event and respond within 60 seconds, or else the request will time out in Ditto.

## Signing with Soapbox

![Ditto Signing Soapbox](./img/ditto-sign-soapbox.svg)

Soapbox implements full NIP-46 signing capabilities. It can sign events using a browser extension, or a keypair stored in the browser's local storage.

When logging in with Soapbox, Soapbox automatically constructs the NIP-46 bunker parameters and then sends them to POST `/oauth/authorize` with a `nostr_bunker` grant_type. This endpoint grants Soapbox a bearer token in the same way as above.

This allows seamless authorization between Soapbox and Ditto, so the user isn't aware it's using NIP-46 at all.