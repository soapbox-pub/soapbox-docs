# Syncing Events

When you start your Ditto server for the first time, it will be empty.
If you add [firehose relays](/ditto/install#add-relays) you will start to see content eventually, but you can speed up the process by syncing events to Ditto's relay yourself.

## Install nak

[nak](https://github.com/fiatjaf/nak) is an essential tool to managing a Nostr relay like Ditto.
The easiest way to install it is to download a [precompiled binary](https://github.com/fiatjaf/nak/releases) of the latest release.

Or use this one-liner:

```sh
curl -L -o /usr/local/bin/nak "$(curl -s https://api.github.com/repos/fiatjaf/nak/releases/latest | jq -r '.tag_name' | awk '{print "https://github.com/fiatjaf/nak/releases/download/" $0 "/nak-" $0 "-linux-amd64"}')" && chmod +x /usr/local/bin/nak
```

## Downloading Profiles

The simplest way to download user profiles from the network is to use nak to sync data from purplepages:

```sh
nak req --paginate purplepag.es | nak event ws://localhost:4036/relay
```

> [!TIP]
> Run this command within the Ditto host to avoid Ditto's rate-limiting.

## Downloading Posts

A similar method can be used to pull in text notes:

```sh
nak req -k 1 --paginate relay.primal.net | nak event ws://localhost:4036/relay
```