# Ditto Translations

Ditto supports post translations, as shown in the image below:

![Example translated post](./img/ditto-translation-example.png)

The current supported providers are:

- [`DeepL`](https://deepl.com/)
- [`LibreTranslate`](https://libretranslate.com/)

## DeepL as translation provider
Set the following environment variables:
- `TRANSLATION_PROVIDER=deepl`
- `DEEPL_BASE_URL` - *Base* URL endpoint
- `DEEPL_API_KEY` - API KEY

## LibreTranslate as translation provider
Set the following environment variables:
- `TRANSLATION_PROVIDER=libretranslate`
- `LIBRETRANSLATE_BASE_URL` - *Base* URL endpoint
- `LIBRETRANSLATE_API_KEY` - API KEY

::: tip
You can keep both the **API KEYS** and **URLs endpoints** of different providers set at the same time, the only environment variable that must be uniquely set is `TRANSLATION_PROVIDER`
:::
