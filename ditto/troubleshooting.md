# Troubleshooting Ditto

Here are some troubleshooting tips, including some common problems you may run into while running Ditto.

## Viewing Logs

You can view active Ditto logs by running:

```sh
journalctl -fu ditto
```

To view past logs, try:

```sh
journalctl -u ditto --since "yesterday"    # (or "1 hour ago", etc.)
```

Finally, you can filter logs with `grep`:

```sh
journalctl -fu ditto | grep "error"
```

### Verbose Logging with `DEBUG`

Edit `.env` and add `DEBUG=*` on a separate line. This will enable verbose logging. Now restart Ditto (`systemctl restart ditto`) and check the logs again (`journalctl -fu ditto`) to see verbose logs.

## Debugging Soapbox

If the error is in the browser, you may want to inspect Soapbox.

Press `Shift+Ctrl+i` (or right-click > "Inspect element" somewhere on the page) to bring up the web inspector.

There are two tabs of importance:

- **Network** - shows network requests, with failed ones in red.
- **Console** - shows JavaScript errors.

First look at the "Network" tab and see if there are any results in red. What are they? Are only some failed or are they all failed? Click them and look at their URL. You can click a response, and then choose "Preview" to see the data.

Next check the "Console" for further hints. Are the same network errors also shown in the console? Are there any messages above or below them?

## Blank White Webpage

If you see a blank white page when visiting your website, that means something failed early in the life of the page, such as static HTML and JavaScript files not being loaded.

Check the Network tab of your inspector. A common cause is [CSP](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) errors, which indicates that your server is misconfigured. Check the `LOCAL_DOMAIN` setting in `.env` and ensure it matches the URL of your website.

## Cloudflare SSL

If you are seeing "Too many redirects" or another SSL error, you may have Cloudflare misconfigured.

In Cloudflare, navigate to your domain name, then visit "SSL/TLS" > "Overview" from the left sidebar. In the setting titled "SSL/TLS encryption" click the "Confgure" button.

- If you followed the install guide and used `certbot` to install SSL certificates with Nginx, you need to set this option to **Full** in Cloudflare.
- If you didn't set up SSL, **Flexible** is the right choice. But we recommend adding your own SSL certs so you're not reliant on Cloudflare.

## 429 Rate Limiting Errors

If you're getting 429 errors while browsing the site normally, check the [Rate Limiting](/ditto/rate-limiting) guide and ensure that any proxy servers above Ditto are setting the `X-Real-IP` header correctly. If this header contains the IP of a proxy server instead of the end-user's IP, then requests from anywhere will be treated as coming from the same IP.