---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: Soapbox
  text: Powering the future of social media.
  image: '/img/soapbox.svg'
  actions:
    - theme: brand
      text: Get Started
      link: /ditto/install
    - theme: alt
      text: Source Code
      link: https://gitlab.com/soapbox-pub

features:
  - icon: ⚡
    title: Nostr
    details: Install Soapbox with Ditto to create your own Nostr server.
    link: /ditto/install
  - icon: 🐘
    title: Mastodon
    details: Install Soapbox atop Mastodon.
    link: /soapbox/install/mastodon
  - icon: 🦊
    title: Pleroma
    details: Install Soapbox on Pleroma.
    link: /soapbox/install/pleroma
  - icon: 👷‍♀️
    title: Custom Backend
    details: Create a custom backend to use with Soapbox.
    link: /soapbox/development/backend
  - icon: 📈
    title: Deploy at Scale
    details: Deploy Soapbox for high availability.
    link: /soapbox/administration/deploy-at-scale
  - icon: 🌉
    title: Bridge Across Protocols
    details: Use Mostr to connect between ActivityPub, Nostr, and Bluesky.
    link: /mostr/
---

