# Mostr

![Mostr Architecture](./img/mostr-architecture.svg)

The Mostr Bridge connects ActivityPub and Nostr together, allowing bidirectional communication between the two networks.

## Video

Nostrasia 2023: The Mostr Bridge

<iframe title="The Mostr Bridge | Alex Gleason | Nostrasia 2023" width="560" height="315" src="https://poast.tv/videos/embed/99131b87-46b8-4b1f-bc8c-a019892f933f?warningTitle=0&amp;p2p=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
