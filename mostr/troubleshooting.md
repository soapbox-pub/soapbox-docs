# Troubleshooting

Usually the bridge should "just work" automatically, without users needing to do anything. However, below are some tips in case you're having trouble.

## From Nostr

If you are a Nostr user and not seeing ActivityPub content:

1. **Have at least one relay added from the list on [mostr.pub](https://mostr.pub).** The main relay goes down sometimes, and it's possible the others listed will be more reliable, so try a different relay.

2. **Follow the users you are trying to see content from.** Follow relationships are important on the ActivityPub side, and can affect deliverability of content.

3. **Make sure their server doesn't block mostr.pub.** Usually servers will publish their blocklists, but sometimes they don't.

4. **Only Mastodon, Pleroma (+forks), and Misskey are tested.** We do not support Lemmy and others yet.

## From ActivityPub

If you're an ActivityPub user:

1. **Make sure you follow Nostr users you want to see content from.** You may only see new posts from users after the point you followed them.

2. **Be aware that Nostr users have to publish to a Mostr relay to see their content.** Nostr users may choose not to publish to Mostr.

## Found a bug?

If you think you've found a bug, please report it on the [issue tracker](https://gitlab.com/soapbox-pub/mostr/-/issues).
