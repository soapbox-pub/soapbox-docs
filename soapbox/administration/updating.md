# Updating Soapbox

You should always check the [release notes/changelog](https://gitlab.com/soapbox-pub/soapbox/-/blob/main/CHANGELOG.md) in case there are deprecations, special update changes, etc.

Besides that, it's relatively pretty easy to update Soapbox. Just download the new release and unpack the files.

## Updating with the command line

To update Soapbox via the command line, do the following:

```sh
# Download the build.
curl -O https://dl.soapbox.pub/main/soapbox.zip

# Unzip the new build to where Soapbox was installed before.
# For example, with Pleroma:
busybox unzip soapbox.zip -o -d /opt/pleroma/instance/static
```

## After updating Soapbox

The changes take effect immediately, just refresh your browser tab.