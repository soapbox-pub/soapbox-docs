# Translations

Native speakers from all around the world are welcome to help translate Soapbox into their language.

## Weblate

Translations are managed in [Soapbox Weblate](https://hosted.weblate.org/projects/soapbox-pub/soapbox/).
Weblate allows anyone to help translate without programming skill.

## Translation Files

Locale files in JSON are located in the `src/locales` directory.
These files may be edited directly, as long as maintainers regularly sync changes with Weblate.

## Syncing with Weblate

Maintainers should ensure Weblate translations don't get out of date. To do this, they can add the weblate git repo as a remote and merge changes from it.

First, add the Weblate remote (only do this once):

```sh
git remote add weblate https://hosted.weblate.org/git/soapbox-pub/soapbox/
```

Then create a new branch based off `main` and merge weblate changes into it:

```sh
# Be on the main branch, ensure it's clean
git checkout main
git fetch origin
git reset --hard origin/main

# Create a new branch and merge weblate changes
git fetch weblate
git checkout -b sync-weblate
git merge weblate/main
```

Finally, push the new branch to GitLab and open a merge request.